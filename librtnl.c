#include <errno.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <syslog.h>
#include <sys/socket.h>
#include <linux/rtnetlink.h>
#include <linux/netlink.h>
#include <net/if.h>

#include "locallib.h"

int rtnl_listen_sock(void)
{
	int fd;

	fd = socket(AF_NETLINK, SOCK_RAW | SOCK_NONBLOCK | SOCK_CLOEXEC, NETLINK_ROUTE);
	if (fd < 0)
		mylog(LOG_ERR, "open netlink socket: %s", ESTR(errno));

	int sndbuf = 32768;
	int rcvbuf = 32768;
	setsockopt(fd, SOL_SOCKET, SO_SNDBUF, (void *)&sndbuf, sizeof(sndbuf));
	setsockopt(fd, SOL_SOCKET, SO_RCVBUF, (void *)&rcvbuf, sizeof(rcvbuf));

	struct sockaddr_nl local = {
		.nl_family = AF_NETLINK,
		.nl_pid = getpid(),
		.nl_groups = RTMGRP_LINK,
	};
	if (bind(fd, (void *)&local, sizeof(local)) < 0)
		mylog(LOG_ERR, "bind netlink socket: %s", ESTR(errno));
	return fd;
}

static void parse_rtattr(struct rtattr **tb, int max, struct rtattr *rta, int len)
{
	memset(tb, 0, sizeof(*tb) * max);
	while (RTA_OK(rta, len)) {
		if (rta->rta_type <= max) {
			tb[rta->rta_type] = rta;
		}

		rta = RTA_NEXT(rta, len);
	}
}

void rtnl_link_change_handler(int sock, void *dat)
{
	static uint8_t nlbuf[1024 * 8];
	struct nlmsghdr *h;
	ssize_t len;
	struct ifinfomsg *ifi;
	struct rtattr *tb[IFLA_MAX + 1];
	const char *iface;

	for (;;) {
		len = recv(sock, nlbuf, sizeof(nlbuf), MSG_DONTWAIT);
		if (len < 0) {
			if (errno == EINTR)
				continue;
			else if (errno == EAGAIN)
				break;
			mylog(LOG_ERR, "recv rtnetlink: %s", ESTR(errno));
		}
		for (h = (void *)nlbuf; NLMSG_OK(h, len); h = NLMSG_NEXT(h, len)) {
			if (h->nlmsg_type == NLMSG_DONE)
				return;
			if (h->nlmsg_type != RTM_NEWLINK)
				continue;
			ifi = NLMSG_DATA(h);
			memset(tb, 0, sizeof(tb));
			parse_rtattr(tb, IFLA_MAX, IFLA_RTA(ifi), h->nlmsg_len - NLMSG_LENGTH(sizeof(*ifi)));
			iface = tb[IFLA_IFNAME] ? RTA_DATA(tb[IFLA_IFNAME]) : NULL;

			iface_changed(iface, ifi->ifi_flags);
		}
	}
}
