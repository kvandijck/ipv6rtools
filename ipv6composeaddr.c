#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <endian.h>
#include <getopt.h>
#include <syslog.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "locallib.h"

#define NAME "ipv6composeaddr"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

/* program options */
static const char help_msg[] =
	NAME ": emit ipv6 stateless-autoconfiguration address\n"
	"usage:	" NAME " [OPTIONS ...] MAC NETWORK\n"
	"usage:	" NAME " [OPTIONS ...] -6 NETWORK CLIENT\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	" -6,			compose NETWORK and CLIENT addr\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },
	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "Vv?6";

static int type;
	#define TYPE_MAC_NETWORK	0
	#define TYPE_NETWORK_CLIENT	1

static uint64_t strtohex_drop_unknown(const char *cstr)
{
	uint64_t result = 0;
	int nibble;

	for (; *cstr; ++cstr) {
		if (*cstr >= '0' && *cstr <= '9')
			nibble = *cstr - '0';
		else if (*cstr >= 'a' && *cstr <= 'f')
			nibble = *cstr - 'a' + 10;
		else if (*cstr >= 'A' && *cstr <= 'F')
			nibble = *cstr - 'A' + 10;
		else
			continue;
		result = (result << 4) | nibble;
	}
	return result;
}

int main(int argc, char *argv[])
{
	int opt;
	int loglevel = LOG_WARNING;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\nCompiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case 'v':
		++loglevel;
		break;
	case '6':
		type = TYPE_NETWORK_CLIENT;
		break;

	default:
		fprintf(stderr, "unknown option '%c'\n", opt);
	case '?':
		fputs(help_msg, stderr);
		exit(1);
		break;
	}

	myopenlog(NAME, 0, LOG_LOCAL2);
	myloglevel(loglevel);

	if (optind + 2 != argc)
		mylog(LOG_ERR, "wrong parameters, run with -?");

	char *network;

	/* assign nets */
	union {
		struct in6_addr in6;
		uint64_t i64[2];
	} addr;
	uint64_t client_bin, mac;

	switch (type) {
	case TYPE_MAC_NETWORK:
		network = argv[optind+1];
		mac = strtohex_drop_unknown(argv[optind]);
		client_bin = htobe64((0x000000fffe000000ULL | ((mac & 0xffffff000000ULL) << 16) | (mac & 0x000000ffffffULL)) ^ 0x0200000000000000ULL);
		break;
	case TYPE_NETWORK_CLIENT:
		network = argv[optind];
		if (!inet_pton(AF_INET6, argv[optind+1], &addr.in6))
			mylog(LOG_ERR, "bad ipv6 addr '%s'", argv[optind+1]);
		client_bin = addr.i64[1];
		break;
	}

	if (!inet_pton(AF_INET6, network, &addr.in6))
		mylog(LOG_ERR, "bad ipv6 addr '%s'", network);

	addr.i64[1] = client_bin;

	char buffer[128];

	inet_ntop(AF_INET6, &addr.in6, buffer, sizeof(buffer));
	printf("%s\n", buffer);
	return 0;
}
