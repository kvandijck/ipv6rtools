/*
 * Kurt Van Dijck <dev.kurt@vandijck-laurijssen.be>, 2018
 *
 *   Authors:
 *    Lars Fenneberg		<lf@elemental.net>
 *    Marko Myllynen		<myllynen@lut.fi>
 *
 *   This software is Copyright 1996-2000 by the above mentioned author(s),
 *   All Rights Reserved.
 *
 *   The license which is distributed with this software in the file COPYRIGHT
 *   applies to this software. If your distribution is missing this file, you
 *   may request it from <reubenhwk@gmail.com>.
 *
 */
#include <ctype.h>
#include <errno.h>
#include <math.h>
#include <signal.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <poll.h>
#include <syslog.h>
#include <sys/signalfd.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>
#include <netpacket/packet.h>

#include "libet/libt.h"
#include "libet/libe.h"
#include "locallib.h"

#define NAME "ipv6ra-dns"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

#define CLIENT "/etc/rc.ipv6radns"

/* program options */
static const char help_msg[] =
	NAME ": fetch DNS info from ipv6 RA\n"
	"usage:	" NAME " [OPTIONS ...]\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	" -s, --script=SCRIPT	execute SCRIPT on reception of new info (default " CLIENT ")\n"
	" -t, --test		run test mode\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },

	{ "script", required_argument, NULL, 's', },
	{ "test", no_argument, NULL, 't', },

	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "Vv?s:t";

static char *script = CLIENT;
static sigset_t orig_sigmask;

/* signalling */
static int termreq;

static void sigfd_handler(int fd, void *dat)
{
	int ret;
	struct signalfd_siginfo sfdi;
	int status;

	for (;;) {
		/* repeat until all signals have been read, possibe because of NONBLOCK */
		ret = read(fd, &sfdi, sizeof(sfdi));
		if (ret < 0 && errno == EAGAIN)
			break;
		if (ret < 0)
			mylog(LOG_ERR, "read signalfd: %s", ESTR(errno));
		switch (sfdi.ssi_signo) {
		case SIGTERM:
		case SIGINT:
			termreq = 1;
			break;
		case SIGCHLD:
			for (;;) {
				/* flush zombie process info */
				ret = waitpid(-1, &status, WNOHANG);
				if (ret < 0 && errno == ECHILD)
					break;
				if (ret < 0 && errno == EINTR)
					continue;
				if (ret < 0)
					mylog(LOG_ERR, "waitpid: %s", ESTR(errno));
			}
			break;
		}
	}
}

/* Note: these are applicable to receiving sockopts only */
#if defined IPV6_PKTINFO && !defined IPV6_RECVPKTINFO
#define IPV6_RECVPKTINFO IPV6_PKTINFO
#endif
#undef ND_OPT_RDNSS_INFORMATION
#define ND_OPT_RDNSS_INFORMATION 25
#undef ND_OPT_DNSSL_INFORMATION
#define ND_OPT_DNSSL_INFORMATION 31

int open_icmpv6_socket(void)
{
	int sock;
	struct icmp6_filter filter;
	int err;

	sock = socket(AF_INET6, SOCK_RAW | SOCK_NONBLOCK | SOCK_CLOEXEC, IPPROTO_ICMPV6);
	if (sock < 0)
		mylog(LOG_ERR, "can't create socket(AF_INET6): %s", strerror(errno));

	err = setsockopt(sock, IPPROTO_IPV6, IPV6_RECVPKTINFO, (int[]){1}, sizeof(int));
	if (err < 0)
		mylog(LOG_ERR, "setsockopt(IPV6_RECVPKTINFO): %s", strerror(errno));

#ifdef __linux__
	err = setsockopt(sock, IPPROTO_RAW, IPV6_CHECKSUM, (int[]){2}, sizeof(int));
#else
	err = setsockopt(sock, IPPROTO_IPV6, IPV6_CHECKSUM, (int[]){2}, sizeof(int));
#endif
	if (err < 0)
		mylog(LOG_ERR, "setsockopt(IPV6_CHECKSUM): %s", strerror(errno));

	err = setsockopt(sock, IPPROTO_IPV6, IPV6_UNICAST_HOPS, (int[]){255}, sizeof(int));
	if (err < 0)
		mylog(LOG_ERR, "setsockopt(IPV6_UNICAST_HOPS): %s", strerror(errno));

	err = setsockopt(sock, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, (int[]){255}, sizeof(int));
	if (err < 0)
		mylog(LOG_ERR, "setsockopt(IPV6_MULTICAST_HOPS): %s", strerror(errno));

	/* setup ICMP filter */
	ICMP6_FILTER_SETBLOCKALL(&filter);
	ICMP6_FILTER_SETPASS(ND_ROUTER_ADVERT, &filter);

	err = setsockopt(sock, IPPROTO_ICMPV6, ICMP6_FILTER, &filter, sizeof(filter));
	if (err < 0)
		mylog(LOG_ERR, "setsockopt(ICMPV6_FILTER): %s", strerror(errno));

	return sock;
}

static char **cache;
static int ncache, scache;

static const char *const envs[] = {
	"IFACE",
	"ROUTER",
	"RDNSS1",
	"RDNSS2",
	"RDNSS3",
	"DNSSL1",
	"DNSSL2",
	"DNSSL3",
	"DNSSL4",
};
#define NITEMS	(sizeof(envs)/sizeof(*envs))
#define NRDNSS 3
#define NDNSSL 4

static int is_different(const char *iface, const char *router)
{
	char **lp;
	int j;

	if (!iface || !strcmp(iface, ""))
		return 0;
	if (!cache)
		goto initial;
	/* find current IFACE info in cache */
	for (lp = cache; lp < cache+ncache && *lp; lp += NITEMS) {
		/* ignore if iface _OR_ router are not different */
		if (strcmp(iface, lp[0]) || strcmp(router, lp[1]))
			continue;
		for (j = 2; j < NITEMS; ++j) {
			if (strcmp(getenv(envs[j]) ?: "", lp[j] ?: ""))
				goto propagate;
		}
		/* all items match */
		return 0;
	}

propagate:
	if (!*lp) {
initial:
		/* append */
		if (ncache + NITEMS > scache) {
			scache += NITEMS*4;
			cache = realloc(cache, scache*sizeof(*cache));
			if (!cache)
				mylog(LOG_ERR, "realloc %u: %s", scache, ESTR(errno));
		}
		for (j = 0; j < NITEMS; ++j)
			cache[ncache+j] = strdup(getenv(envs[j]) ?: "");
		ncache += NITEMS;
	} else {
		/* replace info */
		for (j = 2; j < NITEMS; ++j) {
			free(lp[j]);
			lp[j] = strdup(getenv(envs[j]) ?: "");
		}
	}
	return 1;
}

static int log_this(const char *iface, const char *router)
{
	/* log info when script is absent */
	int j;
	int ndone;
	const char *str;
	for (ndone = 0, j = 0; j < NITEMS; ++j) {
		str = getenv(envs[j]);
		if (str) {
			mylog(LOG_NOTICE, "%s %s: %s=%s", iface, router, envs[j], str);
			++ndone;
		}
	}
	return ndone;
}

static void run_script(const char *iface, const char *router)
{
	int pid;

	if (!router) {
		char **lp;
		/* find current IFACE info in cache */
		for (lp = cache; *lp; lp += NITEMS) {
			if (!strcmp(iface, cache[0]) && cache[1])
				/* found 1 */
				run_script(iface, cache[1]);
		}
		return;
	}
	/* log info when script is absent */
	int cnt = log_this(iface, router);
	if (cnt <= 2)
		mylog(LOG_NOTICE, "%s %s: no dns info", iface, router);

	/* test script for executing,
	 * avoid spawning if we know right now that we will fail
	 */
	static int last_errno;
	if (access(script, X_OK) < 0) {
		if (errno != last_errno) {
			last_errno = errno;
			mylog(LOG_WARNING, "run %s: %s", script, ESTR(errno));
		}
		return;
	}
	last_errno = 0;

	pid = fork();
	if (pid < 0)
		mylog(LOG_ERR, "fork: %s", ESTR(errno));
	if (!pid) {
		if (sigprocmask(SIG_SETMASK, &orig_sigmask, NULL) < 0)
			mylog(LOG_WARNING, "sigprocmask failed: %s", ESTR(errno));
		execlp(script, script, NULL);
		mylog(LOG_ERR, "execlp %s: %s", script, ESTR(errno));
	}
}

static void on_ra(const void *vdat, int len, struct sockaddr_in6 *peer, struct in6_pktinfo *pktinfo)
{
	const uint8_t *dat = vdat;
	static char peername[256], addrname[256], dnsname[256], ifnam[64];
	char *str;
	int opt, optlen, dnslen, pos, j;
	int nrdnss, ndnssl;

	dat += sizeof(struct nd_router_advert);
	len -= sizeof(struct nd_router_advert);

	inet_ntop(peer->sin6_family, &peer->sin6_addr, peername, sizeof(peername));

	if (!if_indextoname(pktinfo->ipi6_ifindex, ifnam))
		ifnam[0] = 0; /* null-terminate ifnam */
	setenv("IFACE", ifnam, 1);
	setenv("ROUTER", peername, 1);
	for (j = 2; j < NITEMS; ++j)
		unsetenv(envs[j]);

	nrdnss = ndnssl = 0;
	for (; len > 0; dat += optlen, len -= optlen) {
		if (len < 2) {
			mylog(LOG_WARNING, "trailing garbage from %s", peername);
			break;
		}
		opt = *dat;
		optlen = dat[1] << 3;
		if (optlen > len) {
			mylog(LOG_WARNING, "option len %u > total len %u, for type %u",
					optlen, len, opt);
			continue;
		}
		switch (opt) {
		case ND_OPT_RDNSS_INFORMATION:
			if (optlen < 24) {
				mylog(LOG_WARNING, "option len %u too small for rdnss", optlen);
				break;
			}
			for (pos = 8; pos < optlen; pos += 16) {
				if (nrdnss >= NRDNSS) {
					mylog(LOG_WARNING, ">%u RDNSS entries", nrdnss);
					break;
				}
				inet_ntop(AF_INET6, dat+pos, addrname, sizeof(addrname));
				setenv(envs[2+nrdnss], addrname, 1);
				++nrdnss;
			}
			break;
		case ND_OPT_DNSSL_INFORMATION:
			if (ndnssl >= NDNSSL) {
				mylog(LOG_WARNING, ">%u DNSSL entries", ndnssl);
				break;
			}
			if (optlen < 8) {
				mylog(LOG_WARNING, "option len %u too small for dnssl", optlen);
				break;
			}
			str = dnsname;
			for (pos = 8; pos < optlen; ) {
				dnslen = dat[pos++];
				if ((str-dnsname)+dnslen+2 > sizeof(dnsname)) {
					mylog(LOG_WARNING, "dnssl entry oversized");
					goto dnssl_done;
				}
				if (!dnslen) {
					if (str > dnsname) {
						setenv(envs[5+ndnssl], dnsname, 1);
						++ndnssl;
						str = dnsname;
						*str = 0;
					}
					continue;
				}
				if (str > dnsname)
					*str++ = '.';
				strncpy(str, (char *)&dat[pos], dnslen);
				pos += dnslen;
				str += dnslen;
				*str = 0;
			}
dnssl_done:
			break;
		}

	}

	if (is_different(ifnam, peername))
		run_script(ifnam, peername);
	else
		mylog(LOG_DEBUG, "%s %s: repeated", ifnam, peername);
}

static void icmpv6_data(int sock, void *ptr)
{
	int ret, len;
	static uint8_t dat[1500];
	static uint8_t chdr[CMSG_SPACE(sizeof(struct in6_pktinfo))];
	struct sockaddr_in6 peer;
	struct in6_pktinfo *pktinfo;
	struct icmp6_hdr *icmp;

	for (;;) {
		struct iovec iov = {
			.iov_base = dat,
			.iov_len = sizeof(dat),
		};
		struct msghdr msg = {
			.msg_name = &peer,
			.msg_namelen = sizeof(peer),
			.msg_iov = &iov,
			.msg_iovlen = 1,
			.msg_control = chdr,
			.msg_controllen = sizeof(chdr),
		};

		len = ret = recvmsg(sock, &msg, MSG_DONTWAIT);
		if (ret < 0) {
			if (errno == EAGAIN)
				/* no more data */
				break;
			else if (errno == EINTR)
				continue;
			mylog(LOG_ERR, "recvmsg: %s", ESTR(errno));
		}
		/* parse control data */
		pktinfo = NULL;
		struct cmsghdr *cmsg;
		for (cmsg = CMSG_FIRSTHDR(&msg); cmsg != NULL; cmsg = CMSG_NXTHDR(&msg, cmsg)) {
			if (cmsg->cmsg_level != IPPROTO_IPV6)
				continue;
			if (cmsg->cmsg_type == IPV6_PKTINFO) {
				if (cmsg->cmsg_len >= CMSG_LEN(sizeof(*pktinfo)))
					pktinfo = (void *)CMSG_DATA(cmsg);
				else
					mylog(LOG_WARNING, "bad ipv6 pktinfo recvd");
			}
		}
		if (!pktinfo)
			mylog(LOG_ERR, "no ipv6 pktinfo recvd");

		if (!len)
			mylog(LOG_ERR, "recvmsg: EOF");

		if (len < sizeof(*icmp))
			mylog(LOG_ERR, "recvd icmpv6 packet length %u, expected %u", len, (int)sizeof(*icmp));

		icmp = (void *)dat;

		if (icmp->icmp6_type == ND_ROUTER_ADVERT) {
			on_ra(dat, len, &peer, pktinfo);
		} else
			mylog(LOG_ERR, "icmpv6 filter failed, recvd type %u", icmp->icmp6_type);
	}
}

void iface_changed(const char *iface, int flags)
{
	int j, i;

	if (!iface)
		return;
	if (flags & IFF_RUNNING)
		return;
	/* find current IFACE info in cache */
	for (i = 0; i < ncache; i += NITEMS) {
		if (!strcmp(iface, cache[i])) {
			for (j = 1; j < NITEMS; ++j) {
				if (cache[i+j])
					goto must_remove;
			}
			break;
		}
	}
	/* if we come here, we are done */
	return;

must_remove:
	mylog(LOG_NOTICE, "%s: lost connection", iface);
	for (j = 0; j < NITEMS; ++j) {
		if (cache[i+j])
			free(cache[i+j]);
	}
	memmove(cache+i, cache+i+NITEMS, sizeof(*cache)*(ncache-(i+NITEMS)));
	ncache -= NITEMS;
	/* clear tail */
	memset(cache+ncache, 0, sizeof(*cache)*NITEMS);

	setenv(envs[0], iface, 1);
	for (j = 1; j < NITEMS; ++j)
		unsetenv(envs[j]);
	run_script(iface, NULL);
}

static const char *const tests[] = {
	"eth0", "rtr1", "rdnss1", "rdnss2", NULL, "dnssl1", "dnssl2", NULL, NULL,
	"eth1", "rtr1", "rdnss1", "rdnss2", NULL, "dnssl1", "dnssl2", NULL, NULL,
	"eth0", "rtr1", "rdnss1", "rdnss2", NULL, "dnssl1", "dnssl2", NULL, NULL,
	"eth1", "rtr1", "rdnss1", "rdnss2", NULL, "dnssl1", "dnssl2", NULL, NULL,
	"eth0", "rtr2", "rdnsa1", "rdnsa2", NULL, "dnssa1", "dnssa2", NULL, NULL,
	"eth0", "rtr1", "rdnss1", "rdnss2", NULL, "dnssl1", "dnssl2", NULL, NULL,
	"eth0", "rtr2", "rdnsa1", "rdnsa2", NULL, "dnssa1", "dnssa2", NULL, NULL,
	NULL,
};

static void runtests(void)
{
	const char *const *lp;
	int j;

	for (lp = tests; *lp; lp += NITEMS) {
		for (j = 0; j < NITEMS; ++j) {
			if (lp[j])
				setenv(envs[j], lp[j], 1);
			else
				unsetenv(envs[j]);
		}
		if (is_different(lp[0], lp[1]))
			log_this(lp[0], lp[1]);
		else
			mylog(LOG_DEBUG, "%s %s: repeated", lp[0], lp[1]);
	}
	exit(0);
}

int main(int argc, char *argv[])
{
	int opt, ret;
	int loglevel = LOG_NOTICE;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\nCompiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case 'v':
		++loglevel;
		break;
	case 's':
		script = optarg;
		break;

	case 't':
		/* test mode */
		myopenlog(NAME, 0, LOG_LOCAL2);
		myloglevel(loglevel);
		runtests();
		break;

	default:
		fprintf(stderr, "unknown option '%c'\n", opt);
	case '?':
		fputs(help_msg, stderr);
		exit(1);
		break;

	}

	myopenlog(NAME, 0, LOG_LOCAL2);
	myloglevel(loglevel);

	/* block async signal delivery */
	sigset_t sigmask;
	sigfillset(&sigmask);
	if (sigprocmask(SIG_BLOCK, &sigmask, &orig_sigmask) < 0)
		mylog(LOG_ERR, "sigprocmask: %s", ESTR(errno));
	/* listen to queued signals */
	int sigfd = signalfd(-1, &sigmask, SFD_NONBLOCK | SFD_CLOEXEC);
	if (sigfd < 0)
		mylog(LOG_ERR, "signalfd failed: %s", ESTR(errno));
	libe_add_fd(sigfd, sigfd_handler, NULL);

	/* get a raw socket for sending and receiving ICMPv6 messages */
	int sock = open_icmpv6_socket();
	if (sock < 0)
		mylog(LOG_ERR, "open icmpv6 socket");
	libe_add_fd(sock, icmpv6_data, NULL);

	sock = rtnl_listen_sock();
	if (sock < 0)
		mylog(LOG_ERR, "open netlink socket");
	libe_add_fd(sock, rtnl_link_change_handler, NULL);

	for (; !termreq;) {
		ret = libe_wait(libt_get_waittime());
		if (ret < 0 && errno == EINTR)
			continue;
		if (ret < 0)
			mylog(LOG_ERR, "libe_wait(): %s", ESTR(errno));
		libe_flush();
		libt_flush();
	}
	return 0;
}
