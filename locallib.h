#ifndef _locallib_h_
#define _locallib_h_
#ifdef __cplusplus
extern "C" {
#endif

/* generic error logging */
#define ESTR(num)	strerror(num)
__attribute__((format(printf,2,3)))
extern void mylog(int loglevel, const char *fmt, ...);
extern void myopenlog(const char *name, int options, int facility);
extern void myloglevel(int loglevel);
extern int mysetloglevelstr(char *str);

/* rtnetlink lost-link detection */
extern int rtnl_listen_sock(void);
extern void rtnl_link_change_handler(int sock, void *dat);
/* to be defined in the program */
extern void iface_changed(const char *iface, int flags);

#ifdef __cplusplus
}
#endif
#endif
