PROGS	= ipv6pd-client modradvdconf
PROGS	+= ipv6detectnew
PROGS	+= ipv6netidx
PROGS	+= ipv6ra-dns
PROGS	+= ipv6composeaddr
default	: $(PROGS)

PREFIX	= /usr/local

CC	= gcc
CFLAGS	= -Wall
CPPFLAGS= -D_GNU_SOURCE
LDLIBS	=
INSTOPTS= -s

VERSION := $(shell git describe --tags --always)

-include config.mk

# avoid overruling the VERSION
CPPFLAGS += -DVERSION=\"$(VERSION)\"

ipv6detectnew: liblog.o libet/libt.o libet/libe.o
ipv6netidx: liblog.o

ipv6ra-dns: LDLIBS:=-lm $(LDLIBS)
ipv6ra-dns: liblog.o libet/libt.o libet/libe.o librtnl.o

ipv6pd-client: LDLIBS:=-lm $(LDLIBS)
ipv6pd-client: libet/libt.o libet/libe.o liblog.o librtnl.o

modradvdconf: liblog.o

ipv6composeaddr: liblog.o

install: $(PROGS)
	$(foreach PROG, $(PROGS), install -vp -m 0777 $(INSTOPTS) $(PROG) $(DESTDIR)$(PREFIX)/bin/$(PROG);)

clean:
	rm -rf $(wildcard *.o libet/*.o) $(PROGS)
