/*
 * Kurt Van Dijck <dev.kurt@vandijck-laurijssen.be>, 2019
 *
 *   Authors:
 *    Lars Fenneberg		<lf@elemental.net>
 *    Marko Myllynen		<myllynen@lut.fi>
 *
 *   This software is Copyright 1996-2000 by the above mentioned author(s),
 *   All Rights Reserved.
 *
 *   The license which is distributed with this software in the file COPYRIGHT
 *   applies to this software. If your distribution is missing this file, you
 *   may request it from <reubenhwk@gmail.com>.
 *
 */
#include <ctype.h>
#include <errno.h>
#include <math.h>
#include <signal.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <poll.h>
#include <syslog.h>
#include <sys/signalfd.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>
#include <netpacket/packet.h>

#include "libet/libt.h"
#include "libet/libe.h"
#include "locallib.h"

#define NAME "ipv6detectnew"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

/* program options */
static const char help_msg[] =
	NAME ": listen to ipv6 Neighbour Solicits\n"
	"usage:	" NAME " [OPTIONS ...]\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	" -s, --script=SCRIPT	execute SCRIPT on reception of new info\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },

	{ "script", required_argument, NULL, 's', },

	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "Vv?s:";

static char *script;
static sigset_t orig_sigmask;

/* signalling */
static int termreq;

static void sigfd_handler(int fd, void *dat)
{
	int ret;
	struct signalfd_siginfo sfdi;
	int status;

	for (;;) {
		/* repeat until all signals have been read, possibe because of NONBLOCK */
		ret = read(fd, &sfdi, sizeof(sfdi));
		if (ret < 0 && errno == EAGAIN)
			break;
		if (ret < 0)
			mylog(LOG_ERR, "read signalfd: %s", ESTR(errno));
		switch (sfdi.ssi_signo) {
		case SIGTERM:
		case SIGINT:
			termreq = 1;
			break;
		case SIGCHLD:
			for (;;) {
				/* flush zombie process info */
				ret = waitpid(-1, &status, WNOHANG);
				if (ret < 0 && errno == ECHILD)
					break;
				if (ret < 0 && errno == EINTR)
					continue;
				if (ret < 0)
					mylog(LOG_ERR, "waitpid: %s", ESTR(errno));
			}
			break;
		}
	}
}

/* Note: these are applicable to receiving sockopts only */
#if defined IPV6_PKTINFO && !defined IPV6_RECVPKTINFO
#define IPV6_RECVPKTINFO IPV6_PKTINFO
#endif
#undef ND_OPT_RDNSS_INFORMATION
#define ND_OPT_RDNSS_INFORMATION 25
#undef ND_OPT_DNSSL_INFORMATION
#define ND_OPT_DNSSL_INFORMATION 31

int open_icmpv6_socket(void)
{
	int sock;
	struct icmp6_filter filter;
	int err;

	sock = socket(AF_INET6, SOCK_RAW | SOCK_NONBLOCK | SOCK_CLOEXEC, IPPROTO_ICMPV6);
	if (sock < 0)
		mylog(LOG_ERR, "can't create socket(AF_INET6): %s", strerror(errno));

	err = setsockopt(sock, IPPROTO_IPV6, IPV6_RECVPKTINFO, (int[]){1}, sizeof(int));
	if (err < 0)
		mylog(LOG_ERR, "setsockopt(IPV6_RECVPKTINFO): %s", strerror(errno));

#ifdef __linux__
	err = setsockopt(sock, IPPROTO_RAW, IPV6_CHECKSUM, (int[]){2}, sizeof(int));
#else
	err = setsockopt(sock, IPPROTO_IPV6, IPV6_CHECKSUM, (int[]){2}, sizeof(int));
#endif
	if (err < 0)
		mylog(LOG_ERR, "setsockopt(IPV6_CHECKSUM): %s", strerror(errno));

	err = setsockopt(sock, IPPROTO_IPV6, IPV6_UNICAST_HOPS, (int[]){255}, sizeof(int));
	if (err < 0)
		mylog(LOG_ERR, "setsockopt(IPV6_UNICAST_HOPS): %s", strerror(errno));

	err = setsockopt(sock, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, (int[]){255}, sizeof(int));
	if (err < 0)
		mylog(LOG_ERR, "setsockopt(IPV6_MULTICAST_HOPS): %s", strerror(errno));

	/* setup ICMP filter */
	ICMP6_FILTER_SETBLOCKALL(&filter);
	ICMP6_FILTER_SETPASS(ND_NEIGHBOR_SOLICIT, &filter);

	err = setsockopt(sock, IPPROTO_ICMPV6, ICMP6_FILTER, &filter, sizeof(filter));
	if (err < 0)
		mylog(LOG_ERR, "setsockopt(ICMPV6_FILTER): %s", strerror(errno));

	return sock;
}

static int is_linklocal(const void *in6_addr)
{
	static const uint8_t v6_ll_prefix[8] = { 0xfe, 0x80, };

	return !memcmp(in6_addr, v6_ll_prefix, sizeof(v6_ll_prefix));
}

static int seems_macaddr_based(const void *in6_addr)
{
	const uint8_t *dat = in6_addr;

	return (dat[11] == 0xff && dat[12] == 0xfe);
}

static const uint8_t *in6_to_mac(const void *in6_addr)
{
	const uint8_t *dat = in6_addr;
	static uint8_t mac[6];

	memcpy(mac+0, dat+8, 3);
	memcpy(mac+3, dat+13, 3);
	mac[0] ^= 0x02;
	return mac;
}

static const char *mac_to_str(const void *mac)
{
	const uint8_t *dat = mac;

	static char buf[64]; /* 18 chars is required */

	sprintf(buf, "%02x:%02x:%02x:%02x:%02x:%02x",
			dat[0], dat[1], dat[2],
			dat[3], dat[4], dat[5]);
	return buf;
}

static void on_nbsol(const void *vdat, int len, struct sockaddr_in6 *peer, struct in6_pktinfo *pktinfo)
{
	__attribute__((unused))
	const void *payload = ((const struct icmp6_hdr *)vdat)+1;
	static char peername[256], ifnam[64];

	if (!is_linklocal(&peer->sin6_addr))
		return;

	inet_ntop(peer->sin6_family, &peer->sin6_addr, peername, sizeof(peername));

	setenv("IFACE", if_indextoname(pktinfo->ipi6_ifindex, ifnam) ?: "", 1);
	setenv("PEER", peername, 1);

	/* payload is actually our address */
	static char addrname[256];
	inet_ntop(peer->sin6_family, payload, addrname, sizeof(addrname));
	setenv("LOCAL", addrname, 1);

	/* export MAC address */
	if (seems_macaddr_based(&peer->sin6_addr))
		setenv("MAC", mac_to_str(in6_to_mac(&peer->sin6_addr)), 1);
	else
		unsetenv("MAC");

	if (!script) {
		static const char *const keys[] = { "IFACE", "LOCAL", "PEER", "MAC", NULL, };
		const char *const *pkey;
		const char *val;
		int n;

		for (pkey = keys, n = 0; *pkey; ++pkey) {
			val = getenv(*pkey);
			if (!val)
				continue;
			printf("%s%s=%s", n ? ", " : "", *pkey, val);
			++n;
		}
		printf("\n");
		fflush(stdout);
		return;
	}
	/* run script */
	/* test script for executing,
	 * avoid spawning if we know right now that we will fail
	 */
	static int last_errno;
	if (access(script, X_OK) < 0) {
		if (errno != last_errno) {
			last_errno = errno;
			mylog(LOG_WARNING, "run %s: %s", script, ESTR(errno));
		}
		return;
	}
	last_errno = 0;

	int pid;
	pid = fork();
	if (pid < 0)
		mylog(LOG_ERR, "fork: %s", ESTR(errno));
	if (!pid) {
		if (sigprocmask(SIG_SETMASK, &orig_sigmask, NULL) < 0)
			mylog(LOG_WARNING, "sigprocmask failed: %s", ESTR(errno));
		execlp(script, script, NULL);
		mylog(LOG_ERR, "execlp %s: %s", script, ESTR(errno));
	}
}

static void icmpv6_data(int sock, void *ptr)
{
	int ret, len;
	static uint8_t dat[1500];
	static uint8_t chdr[CMSG_SPACE(sizeof(struct in6_pktinfo))];
	struct sockaddr_in6 peer;
	struct in6_pktinfo *pktinfo;
	struct icmp6_hdr *icmp;

	for (;;) {
		struct iovec iov = {
			.iov_base = dat,
			.iov_len = sizeof(dat),
		};
		struct msghdr msg = {
			.msg_name = &peer,
			.msg_namelen = sizeof(peer),
			.msg_iov = &iov,
			.msg_iovlen = 1,
			.msg_control = chdr,
			.msg_controllen = sizeof(chdr),
		};

		len = ret = recvmsg(sock, &msg, MSG_DONTWAIT);
		if (ret < 0) {
			if (errno == EAGAIN)
				/* no more data */
				break;
			else if (errno == EINTR)
				continue;
			mylog(LOG_ERR, "recvmsg: %s", ESTR(errno));
		}
		/* parse control data */
		pktinfo = NULL;
		struct cmsghdr *cmsg;
		for (cmsg = CMSG_FIRSTHDR(&msg); cmsg != NULL; cmsg = CMSG_NXTHDR(&msg, cmsg)) {
			if (cmsg->cmsg_level != IPPROTO_IPV6)
				continue;
			if (cmsg->cmsg_type == IPV6_PKTINFO) {
				if (cmsg->cmsg_len >= CMSG_LEN(sizeof(*pktinfo)))
					pktinfo = (void *)CMSG_DATA(cmsg);
				else
					mylog(LOG_WARNING, "bad ipv6 pktinfo recvd");
			}
		}
		if (!pktinfo)
			mylog(LOG_ERR, "no ipv6 pktinfo recvd");

		if (!len)
			mylog(LOG_ERR, "recvmsg: EOF");

		if (len < sizeof(*icmp))
			mylog(LOG_ERR, "recvd icmpv6 packet length %u, expected %u", len, (int)sizeof(*icmp));

		icmp = (void *)dat;

		if (icmp->icmp6_type == ND_NEIGHBOR_SOLICIT) {
			on_nbsol(dat, len, &peer, pktinfo);
		} else
			mylog(LOG_ERR, "icmpv6 filter failed, recvd type %u", icmp->icmp6_type);
	}
}

int main(int argc, char *argv[])
{
	int opt, ret;
	int loglevel = LOG_NOTICE;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\nCompiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case 'v':
		++loglevel;
		break;
	case 's':
		script = optarg;
		if (!strlen(script ?: ""))
			script = NULL;
		break;

	default:
		fprintf(stderr, "unknown option '%c'\n", opt);
	case '?':
		fputs(help_msg, stderr);
		exit(1);
		break;

	}

	myopenlog(NAME, 0, LOG_LOCAL2);
	myloglevel(loglevel);

	if (!script)
		mylog(LOG_INFO, "no-script mode");

	/* block async signal delivery */
	sigset_t sigmask;
	sigfillset(&sigmask);
	if (sigprocmask(SIG_BLOCK, &sigmask, &orig_sigmask) < 0)
		mylog(LOG_ERR, "sigprocmask: %s", ESTR(errno));
	/* listen to queued signals */
	int sigfd = signalfd(-1, &sigmask, SFD_NONBLOCK | SFD_CLOEXEC);
	if (sigfd < 0)
		mylog(LOG_ERR, "signalfd failed: %s", ESTR(errno));
	libe_add_fd(sigfd, sigfd_handler, NULL);

	/* get a raw socket for sending and receiving ICMPv6 messages */
	int sock = open_icmpv6_socket();
	if (sock < 0)
		mylog(LOG_ERR, "open icmpv6 socket");
	libe_add_fd(sock, icmpv6_data, NULL);

	for (; !termreq;) {
		ret = libe_wait(libt_get_waittime());
		if (ret < 0 && errno == EINTR)
			continue;
		if (ret < 0)
			mylog(LOG_ERR, "libe_wait(): %s", ESTR(errno));
		libe_flush();
		libt_flush();
	}
	return 0;
}
