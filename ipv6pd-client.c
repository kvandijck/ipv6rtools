#include <ctype.h>
#include <errno.h>
#include <math.h>
#include <signal.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <endian.h>
#include <fcntl.h>
#include <getopt.h>
#include <poll.h>
#include <syslog.h>
#include <sys/socket.h>
#include <sys/signalfd.h>
#include <sys/wait.h>
#include <linux/if.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <netpacket/packet.h>

#include "libet/libt.h"
#include "libet/libe.h"
#include "locallib.h"

#define NAME "ipv6pd-client"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

#define CLIENT "/etc/rc.dhcpv6pd"

/* program options */
static const char help_msg[] =
	NAME ": dhcpv6 implementation for requesting prefix delegation\n"
	"usage:	" NAME " [OPTIONS ...] IFACE\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"

	" -r, --rapidcommit	Ask for Rapid-Commit\n"
	" -p, --prefix[=WIDTH]	Request prefix delegation (default 64)\n"
	" -A, --noaddr		Don't ask for address\n"
	"			This is only meaningfull with -p\n"
	" -s, --script=SCRIPT	execute SCRIPT on reception of new info (default " CLIENT ")\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },

	{ "prefix", optional_argument, NULL, 'p', },
	{ "rapidcommit", no_argument, NULL, 'r', },
	{ "noaddress", no_argument, NULL, 'A', },
	{ "script", required_argument, NULL, 's', },
	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "Vv?p::rAs:";
static char *script = CLIENT;

static char *iface;
static int debug;

static sigset_t orig_sigmask;
static int sock;
static int prefix;
static int rapidcommit;
static int noaddress;
#define tobe16(x) ((((x) & 0xff) << 8) + (((x) & 0xff00) >> 8))
static struct sockaddr_in6 local = {
	.sin6_family = AF_INET6,
	.sin6_port=  tobe16(546),
}, peer = {
	.sin6_family = AF_INET6,
	.sin6_port=  tobe16(547),
	.sin6_addr.s6_addr = { 0xff, 0x02, 0, 0,  0, 0, 0, 0,  0, 0, 0, 0,  0, 1, 0, 2, },
}, srv;
static const uint8_t empty_ia_na[12] = { 0, 0, 0, 1, };
static const uint8_t empty_ia_pd[12] = { 0, 0, 0, 1, };
static uint8_t dat[2048];

static uint8_t duid[128];
static int duidlen;

static uint8_t svid[128];
static int svidlen;
static int svidrecvd;

static uint8_t myfqdn[128];
static int myfqdnlen;

static int tid;
static double retry = NAN;
static double begintime;

static int state;
#define ST_WAIT_ADVERTISE	1
#define ST_WAIT_REPLY	2

#define MSG_SOLICIT	1
#define MSG_ADVERTISE	2
#define MSG_REQUEST	3
#define MSG_REPLY	7

#define OPTION_CLIENTID	1
#define OPTION_SERVERID	2
#define OPTION_IA_NA	3
#define OPTION_IA_TA	4
#define OPTION_IAADDR	5
#define OPTION_ORO	6
#define OPTION_PREFERENCE	7
#define OPTION_ELAPSED_TIME	8
#define OPTION_STATUS_CODE	13
#define OPTION_RAPID_COMMIT	14
#define OPTION_RECONF_ACCEPT	20
#define OPTION_DNS_SERVERS	23
#define OPTION_DOMAIN_LIST	24
#define OPTION_IA_PD	25
#define OPTION_IAPREFIX	26
#define OPTION_CLIENT_FQDN	39

#define SOL_TIMEOUT	5

static const char *const option_strs[] = {
	[OPTION_CLIENTID] = "CLIENTID",
	[OPTION_SERVERID] = "SERVERID",
	[OPTION_IA_NA] = "IA_NA",
	[OPTION_IA_TA] = "IA_TA",
	[OPTION_IAADDR] = "IAADDR",
	[OPTION_ORO] = "OPTION_REQUEST",
	[OPTION_PREFERENCE] = "OPTION_PREFERENCE",
	[OPTION_ELAPSED_TIME] = "OPTION_ELAPSED_TIME",
	[OPTION_STATUS_CODE] = "STATUS_CODE",
	[OPTION_RAPID_COMMIT] = "RAPID_COMMIT",
	[OPTION_RECONF_ACCEPT] = "RECONF_ACCEPT",
	[OPTION_DNS_SERVERS] = "DNS_SERVERS",
	[OPTION_DOMAIN_LIST] = "DOMAIN_LIST",
	[OPTION_IA_PD] = "IA_PD",
	[OPTION_IAPREFIX] = "IAPREFIX",
	[OPTION_CLIENT_FQDN] = "CLIENT_FQDN",
};

static const char *const msg_strs[16] = {
	[MSG_SOLICIT] = "SOLICIT",
	[MSG_ADVERTISE] = "ADVERTISE",
	[MSG_REQUEST] = "REQUEST",
	[MSG_REPLY] = "REPLY",
};

/* support script environment variables */
static const char *envvar(const char *base, int idx)
{
	static char buf[128];

	if (!idx)
		return base;

	sprintf(buf, "%s%u", base, idx+1);
	return buf;
}

/* helpers */
static int iaddr, iprefix, irdnss;

static void log_env(int nitems, const char *base)
{
	int j;
	const char *key, *value;

	for (j = 0; j < nitems; ++j) {
		key = envvar(base, j);
		value = getenv(key);
		if (value)
			mylog(LOG_NOTICE, "%s: %s=%s", iface, key, value);
	}
}

static void run_script(void)
{
	int pid;

	/* log info */
	log_env(iaddr, "ADDRESS");
	log_env(iprefix, "PREFIX");
	log_env(iprefix, "PREFIX_PRF_LIFETIME");
	log_env(iprefix, "PREFIX_VAL_LIFETIME");
	log_env(irdnss, "RDNSS");

	/* test script for executing,
	 * avoid spawning if we know right now that we will fail
	 */
	static int last_errno;
	if (access(script, X_OK) < 0) {
		if (errno != last_errno) {
			last_errno = errno;
			mylog(LOG_WARNING, "run %s: %s", script, ESTR(errno));
		}
		return;
	}
	last_errno = 0;

	pid = fork();
	if (pid < 0)
		mylog(LOG_ERR, "fork: %s", ESTR(errno));
	if (!pid) {
		if (sigprocmask(SIG_SETMASK, &orig_sigmask, NULL) < 0)
			mylog(LOG_WARNING, "sigprocmask failed: %s", ESTR(errno));
		execlp(script, script, NULL);
		mylog(LOG_ERR, "execlp %s: %s", script, ESTR(errno));
	}
}

/* DHCPv6 ... */
static double new_retry(void)
{
#define IRT	2
#define MRT	30
	if (isnan(retry))
		retry = IRT/2;
	retry = 2*retry + (drand48()-0.5)/5*retry;
	if (retry > MRT)
		retry = MRT + (drand48()-0.5)/5;
	return retry;

}
static void reset_retry(void)
{
	retry = NAN;
}

static void reset_transaction(void)
{
	begintime = NAN;
}
static uint16_t transaction_time(void)
{
	if (isnan(begintime)) {
		begintime = libt_now();
		return 0;
	} else {
		double value;

		value = (libt_now() - begintime)*100;
		if (value > 0xffff)
			return 0xffff;
		else
			return value;
	}
}

static const char *in6tostr(const void *dat)
{
	static char name[1024];
	struct in6_addr addr;

	memcpy(&addr, dat, sizeof(addr));
	inet_ntop(AF_INET6, &addr, name, sizeof(name));
	return name;
}
__attribute__((unused))
static uint16_t getbe16(const void *dat)
{
	uint16_t val;

	memcpy(&val, dat, sizeof(val));
	return ntohs(val);
}
__attribute__((unused))
static uint32_t getbe32(const void *dat)
{
	uint32_t val;

	memcpy(&val, dat, sizeof(val));
	return ntohl(val);
}
__attribute__((unused))
static void putbe16(uint16_t val, void *dat)
{
	val = htons(val);
	memcpy(dat, &val, sizeof(val));
}
__attribute__((unused))
static void putbe32(uint32_t val, void *dat)
{
	val = htons(val);
	memcpy(dat, &val, sizeof(val));
}

static void newduid(void)
{
	int j;

	if (duidlen)
		return;
	memset(duid, 0, sizeof(duid));
	duid[1] = 3;
	duid[3] = 1;
	for (j = 4; j < 10; ++j)
		duid[j] = '0'+j;
	duidlen = 10;
}
static void newtid(uint8_t *dat)
{
	tid = random() & 0xffffff;
	dat[1] = tid >> 16;
	dat[2] = tid >> 8;
	dat[3] = tid;
}
static int readtid(const uint8_t *pkt)
{
	return ((int)pkt[1] << 16) + ((int)pkt[2] << 8) + pkt[3];
}

static int addoption(uint8_t *ptr, int opt, const void *dat, int len)
{
	putbe16(opt, ptr);
	putbe16(len, ptr+2);
	memcpy(ptr+4, dat, len);
	return len+4;
}

static void *findoption(int needle, uint8_t *pkt, int pktlen, int *plen)
{
	int pos = 4;
	int dut, len;

	for (;;) {
		if (pos+4 > pktlen)
			return NULL;
		dut = getbe16(pkt+pos);
		len = getbe16(pkt+pos+2);
		if (dut == needle) {
			*plen = len;
			return pkt+pos+4;
		}
		pkt += 4+len;
	}
}

static void initiate_solicit(void);
static void sendsolicit(void *dummy);
static void advertised(void *dummy);
static void sendrequest(void *dummy);

static void pd_lost(void *dat)
{
	/* loose prefix */
	unsetenv("PREFIX");
	run_script();
}

static void print_status_code(const uint8_t *pkt, int pktlen)
{
	int j;
	/* find start of user message */
	for (j = 1; j < pktlen; ++j) {
		if (isprint(pkt[j]))
			break;
	}
	if (debug)
		fprintf(stderr, "\tSTATUS:%i, %.*s\n", pkt[0], pktlen-j, (char *)(pkt+j));
}
static void loop_print_IA_XA(const uint8_t *pkt, int pktlen)
{
	int pos, opt, len;

	for (pos = 0;;) {
		if (pos+4 > pktlen)
			return;
		opt = getbe16(pkt+pos);
		len = getbe16(pkt+pos+2);

		switch (opt) {
		case OPTION_IAADDR:
			if (len >= 24) {
				if (debug)
					fprintf(stderr, "\t%us\n\t%s\n",
							getbe32(pkt+pos+20),
							in6tostr(pkt+pos+4));
				setenv(envvar("ADDRESS", iaddr++), in6tostr(pkt+pos+4), 1);
			}
			break;
		case OPTION_STATUS_CODE:
			print_status_code(pkt+pos+4, len);
			break;
		}
		pos += 4+len;
	}
}
static void loop_print_IA_PD(const uint8_t *pkt, int pktlen)
{
	int pos, opt, len;
	unsigned int tvalid, tprefer, retry;
	static char buf[128];

	for (pos = 0;;) {
		if (pos+4 > pktlen)
			return;
		opt = getbe16(pkt+pos);
		len = getbe16(pkt+pos+2);

		switch (opt) {
		case OPTION_IAPREFIX:
			if (len >= 25) {
				tprefer = getbe32(pkt+pos+4);
				tvalid = getbe32(pkt+pos+8);
				sprintf(buf, "%s/%u", in6tostr(pkt+pos+13), pkt[pos+12]);
				if (debug)
					fprintf(stderr, "\t%us\t%us\n\t%s\n", tprefer, tvalid, buf);
				setenv(envvar("PREFIX", iprefix), buf, 1);
				sprintf(buf, "%i", tprefer);
				setenv(envvar("PREFIX_PRF_LIFETIME", iprefix), buf, 1);
				sprintf(buf, "%i", tvalid);
				setenv(envvar("PREFIX_VAL_LIFETIME", iprefix), buf, 1);
				++iprefix;
				libt_remove_timeout(pd_lost, NULL);
				if (tprefer > tvalid)
					/* as per rfc, this implies as both 0 */
					tprefer = tvalid = 0;
				retry = tprefer/2;
				if (!retry || retry > 86400)
					/* ceil to 1day */
					retry = 86400;
				if (retry)
					libt_add_timeout(retry, sendsolicit, NULL);
				if (tvalid != ~0 && tvalid)
					libt_add_timeout(tvalid, pd_lost, NULL);
			}
			break;
		case OPTION_STATUS_CODE:
			print_status_code(pkt+pos+4, len);
			break;
		}
		pos += 4+len;
	}
}
static void recvd_reply(const uint8_t *pkt, int pktlen)
{
	int pos, opt, len;
	int j;

	/* clear environment */
	for (j = 0; j < iaddr; ++j)
		unsetenv(envvar("ADDRESS", j));
	for (j = 0; j < iprefix; ++j) {
		unsetenv(envvar("PREFIX", j));
		unsetenv(envvar("PREFIX_PRF_LIFETIME", j));
		unsetenv(envvar("PREFIX_VAL_LIFETIME", j));
	}
	for (j = 0; j < irdnss; ++j)
		unsetenv(envvar("RDNSS", j));
	iaddr = iprefix = irdnss = 0;

	for (pos = 0;;) {
		if (pos+4 > pktlen)
			break;
		opt = getbe16(pkt+pos);
		len = getbe16(pkt+pos+2);
		if (debug)
			fprintf(stderr, "%u+%u,%s\n", opt, len, option_strs[opt] ?: "");
		switch (opt) {
		case OPTION_PREFERENCE:
			if (debug && len >= 1)
				fprintf(stderr, "\t%i\n", pkt[pos+4]);
			break;
		case OPTION_CLIENTID:
		case OPTION_SERVERID:
			if (!debug)
				break;
			fprintf(stderr, "\t");
			for (j = 0; j < len; ++j)
				fprintf(stderr, "%02x", pkt[pos+4+j]);
			fprintf(stderr, ", '");
			for (j = 0; j < len; ++j)
				fprintf(stderr, "%c", isprint(pkt[pos+4+j]) ? pkt[pos+4+j] : '.');
			fprintf(stderr, "'\n");
			break;
		case OPTION_DNS_SERVERS:
			for (j = 0; j < len; j += 16) {
				if (debug)
					fprintf(stderr, "\t%s\n", in6tostr(&pkt[pos+4+j]));
				setenv(envvar("RDNSS", irdnss++), in6tostr(pkt+pos+4+j), 1);
			}
			break;
		case OPTION_IA_NA:
			if (len < 12)
				break;
			if (debug)
				fprintf(stderr, "\t%i,%i,%i\n", getbe32(pkt+pos+4), getbe32(pkt+pos+4+4), getbe32(pkt+pos+4+8));
			loop_print_IA_XA(pkt+pos+4+12, len-12);
			break;
		case OPTION_IA_TA:
			if (len < 4)
				break;
			if (debug)
				fprintf(stderr, "\t%i\n", getbe32(pkt+pos+4));
			loop_print_IA_XA(pkt+pos+4+4, len-4);
			break;
		case OPTION_IA_PD:
			if (len < 12)
				break;
			if (debug)
				fprintf(stderr, "\t%i,%i,%i\n", getbe32(pkt+pos+4), getbe32(pkt+pos+4+4), getbe32(pkt+pos+4+8));
			loop_print_IA_PD(pkt+pos+4+12, len-12);
			break;
		case OPTION_STATUS_CODE:
			print_status_code(pkt+pos+4, len);
			break;
		}
		pos += 4+len;
	}
	if (debug)
		fflush(stderr);
	run_script();
}

static uint8_t *poro; /* base of the oro */
static uint8_t noro; /* # of present oro */
static void prepare_oro(uint8_t *pkt)
{
	poro = pkt;
	noro = 0;
}
static void add_oro(int option)
{
	putbe16(option, &poro[4+noro*2]);
	++noro;
}
static int commit_oro(void)
{
	putbe16(OPTION_ORO, poro);
	putbe16(noro*2, poro+2);
	return 4+noro*2;
}

static void sendsolicit(void *dummy)
{
	uint8_t *ptr = dat;
	uint16_t v16;

	newtid(ptr);
	*ptr = MSG_SOLICIT;
	ptr += 4;
	ptr += addoption(ptr, OPTION_CLIENTID, duid, duidlen);
	if (!noaddress)
	ptr += addoption(ptr, OPTION_IA_NA, &empty_ia_na, sizeof(empty_ia_na));
	prepare_oro(ptr);
	//add_oro(OPTION_IA_NA);
	add_oro(OPTION_DNS_SERVERS);
	add_oro(OPTION_DOMAIN_LIST);
	ptr += commit_oro();

	v16 = htons(transaction_time());
	ptr += addoption(ptr, OPTION_ELAPSED_TIME, &v16, 2);
	if (rapidcommit)
		ptr += addoption(ptr, OPTION_RAPID_COMMIT, NULL, 0);
	ptr += addoption(ptr, OPTION_RECONF_ACCEPT, NULL, 0);
	if (prefix)
		ptr += addoption(ptr, OPTION_IA_PD, &empty_ia_pd, sizeof(empty_ia_pd));
	ptr += addoption(ptr, OPTION_CLIENT_FQDN, myfqdn, myfqdnlen);

	mylog(LOG_INFO, "send %s", msg_strs[dat[0]]);
	if (sendto(sock, dat, ptr-dat, 0, (const void *)&peer, sizeof(peer)) < 0)
		mylog(LOG_ERR, "solicit failed: %s", ESTR(errno));

	libt_add_timeout(new_retry(), sendsolicit, dummy);
	libt_add_timeout(1, advertised, dummy);
	svidrecvd = 0;
	state = ST_WAIT_ADVERTISE;
}
static void advertised(void *dummy)
{
	state = 0;
	if (svidrecvd) {
		libt_remove_timeout(sendsolicit, dummy);
		sendrequest(dummy);
	}
}
static void sendrequest(void *dummy)
{
	uint8_t *ptr = dat;
	uint16_t v16;

	/* select 1 */
	newtid(ptr);
	/* send request message */
	*ptr = MSG_REQUEST;
	ptr += 4;
	ptr += addoption(ptr, OPTION_CLIENTID, duid, duidlen);
	ptr += addoption(ptr, OPTION_SERVERID, svid, svidlen);
	if (!noaddress)
		ptr += addoption(ptr, OPTION_IA_NA, &empty_ia_na, sizeof(empty_ia_na));
	prepare_oro(ptr);
	//add_oro(OPTION_IA_NA);
	add_oro(OPTION_DNS_SERVERS);
	add_oro(OPTION_DOMAIN_LIST);
	ptr += commit_oro();

	v16 = htons(transaction_time());
	ptr += addoption(ptr, OPTION_ELAPSED_TIME, &v16, 2);
	ptr += addoption(ptr, OPTION_RECONF_ACCEPT, NULL, 0);
	if (prefix)
	ptr += addoption(ptr, OPTION_IA_PD, &empty_ia_pd, sizeof(empty_ia_pd));
	ptr += addoption(ptr, OPTION_CLIENT_FQDN, myfqdn, myfqdnlen);

	mylog(LOG_INFO, "send %s", msg_strs[dat[0]]);
	if (sendto(sock, dat, ptr-dat, 0, (const void *)&peer, sizeof(peer)) < 0)
		mylog(LOG_ERR, "request failed: %s", ESTR(errno));

	libt_add_timeout(1, sendrequest, dummy);
	state = ST_WAIT_REPLY;
	/* if case no reply received ... */
	libt_add_timeout(retry, sendsolicit, dummy);
}

static void dhcpv6_handler(int sock, void *ptr)
{
	int ret, len;
	socklen_t slen;
	uint8_t *mptr;

	for (;;) {
		slen = sizeof(srv);
		ret = recvfrom(sock, dat, sizeof(dat), MSG_DONTWAIT, (void *)&srv, &slen);
		if (ret < 0) {
			if (errno == EAGAIN)
				return;
			else if (errno == EINTR)
				continue;
			mylog(LOG_ERR, "recvfrom: %s", ESTR(errno));
		}
		if (!ret)
			mylog(LOG_ERR, "socket closed");

		if (ret < 4) {
			mylog(LOG_WARNING, "short response (%u)", ret);
			continue;
		}
		mylog(LOG_INFO, "recv %s", msg_strs[dat[0]]);
		switch (dat[0]) {
		case MSG_ADVERTISE:
			if (state != ST_WAIT_ADVERTISE)
				goto reject;
			/* validate */
			if (readtid(dat) != tid)
				goto reject;
			mptr = findoption(OPTION_CLIENTID, dat, ret, &len);
			if (!mptr)
				goto reject;
			if (len != duidlen || memcmp(mptr, duid, len))
				goto reject;
			mptr = findoption(OPTION_SERVERID, dat, ret, &len);
			if (!mptr)
				goto reject;
			if (len > sizeof(svid))
				goto reject;
			svidlen = len;
			memcpy(svid, mptr, len);
			reset_retry();
			/* mark as 1 received */
			++svidrecvd;
			break;
		case MSG_REPLY:
			if (state != ST_WAIT_REPLY && (state != ST_WAIT_ADVERTISE || !rapidcommit))
				goto reject;
			/* validate */
			if (readtid(dat) != tid)
				goto reject;
			mptr = findoption(OPTION_CLIENTID, dat, ret, &len);
			if (!mptr)
				goto begin;
			if (len != duidlen || memcmp(mptr, duid, len))
				goto begin;
			mptr = findoption(OPTION_SERVERID, dat, ret, &len);
			if (!mptr)
				goto begin;
			libt_remove_timeout(sendrequest, NULL);
			state = 0;
			libt_remove_timeout(sendsolicit, NULL);
			libt_remove_timeout(advertised, NULL);
			/* process */
			recvd_reply(dat+4, ret-4);
			break;
		}
		if (0) {
reject:
			mylog(LOG_INFO, "recv %s", msg_strs[dat[0]]);
		}
		if (0) {
begin:
			initiate_solicit();
		}
	}
}

static void initiate_solicit(void)
{
	state = 0;
	libt_remove_timeout(pd_lost, NULL);
	libt_remove_timeout(sendrequest, NULL);
	libt_remove_timeout(sendsolicit, NULL);
	libt_remove_timeout(advertised, NULL);

	reset_retry();
	reset_transaction();
	/* start solicit with random delay */
	libt_add_timeout(drand48()*SOL_TIMEOUT, sendsolicit, NULL);
}

void iface_changed(const char *ifnam, int flags)
{
	static int prev_flags;
	static int recvd;

	if (!strcmp(ifnam, iface)) {
		if (recvd && !((prev_flags ^ flags) & IFF_RUNNING))
			/* no change in running state */
			return;
		if (!(flags & IFF_RUNNING)) {
			mylog(LOG_NOTICE, "%s down", iface);
			libt_remove_timeout(pd_lost, NULL);
			libt_remove_timeout(sendrequest, NULL);
			libt_remove_timeout(sendsolicit, NULL);
			libt_remove_timeout(advertised, NULL);
			pd_lost(NULL);
		} else {
			mylog(LOG_NOTICE, "%s up, restart", iface);
			initiate_solicit();
		}
		prev_flags = flags;
		recvd = 1;
	}
}

static void load_runtime_state(const char *iface)
{
	struct ifaddrs *ifa = NULL, *lp;

	if (getifaddrs(&ifa) < 0)
		mylog(LOG_ERR, "getifaddrs failed: %s", ESTR(errno));

	for (lp = ifa; lp; lp = lp->ifa_next) {
		/* take ll address */
		if (!strcmp(iface, lp->ifa_name)) {
			static const uint8_t v6_ll_prefix[8] = { 0xfe, 0x80, };

			if (lp->ifa_addr && lp->ifa_addr->sa_family == AF_INET6) {
				if (!memcmp(&((struct sockaddr_in6 *)lp->ifa_addr)->sin6_addr, v6_ll_prefix, sizeof(v6_ll_prefix))) {
					/* copy link-local address */
					memcpy(&local, lp->ifa_addr, sizeof(local));
					local.sin6_port = tobe16(546);
				}
			}
		}
		/* find duid: first non-lo MAC */
#if 1
		if (!duidlen && (lp->ifa_flags & IFF_UP) && lp->ifa_addr && lp->ifa_addr->sa_family == AF_PACKET) {
			struct sockaddr_ll *ll;
			int j;

			ll = (void *)lp->ifa_addr;
			for (j = 0; j < ll->sll_halen; ++j)
				if (ll->sll_addr[j])
					break;
			if (j < ll->sll_halen) {
				static const uint8_t duid0[4] = { 0, 3, 0, 1, };
				mylog(LOG_INFO, "using DUID from %s", lp->ifa_name);
				duidlen = 4+ll->sll_halen;
				memcpy(duid+4, ll->sll_addr, ll->sll_halen);
				memcpy(duid, duid0, 4);
			}
		}
#endif
	}
	freeifaddrs(ifa);
	/* prepare fqdn option */
	gethostname((char *)myfqdn+2, sizeof(myfqdn)-2);
	myfqdn[0] = 0;
	myfqdn[1] = strlen((char *)myfqdn+2);
	myfqdnlen = 3+myfqdn[1];
}

/* signalling */
static int termreq;

static void sigfd_handler(int fd, void *dat)
{
	int ret;
	struct signalfd_siginfo sfdi;
	int status;

	for (;;) {
		/* repeat until all signals have been read, possibe because of NONBLOCK */
		ret = read(fd, &sfdi, sizeof(sfdi));
		if (ret < 0 && errno == EAGAIN)
			break;
		if (ret < 0)
			mylog(LOG_ERR, "read signalfd: %s", ESTR(errno));
		switch (sfdi.ssi_signo) {
		case SIGTERM:
		case SIGINT:
			termreq = 1;
			break;
		case SIGCHLD:
			for (;;) {
				/* flush zombie process info */
				ret = waitpid(-1, &status, WNOHANG);
				if (ret < 0 && errno == ECHILD)
					break;
				if (ret < 0 && errno == EINTR)
					continue;
				if (ret < 0)
					mylog(LOG_ERR, "waitpid: %s", ESTR(errno));
			}
			break;
		}
	}
}

int main(int argc, char *argv[])
{
	int opt, ret;
	int loglevel = LOG_NOTICE;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\nCompiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case 'v':
		++loglevel;
		debug = loglevel >= LOG_DEBUG;
		break;

	default:
		fprintf(stderr, "unknown option '%c'\n", opt);
	case '?':
		fputs(help_msg, stderr);
		exit(1);
		break;

	case 'p':
		prefix = strtoul(optarg ?: "64", NULL, 0);
		break;
	case 'r':
		rapidcommit = 1;
		break;
	case 'A':
		noaddress = 1;
		break;
	case 's':
		script = optarg;
		break;
	}

	iface = argv[optind++];
	if (!iface) {
		fputs(help_msg, stderr);
		exit(1);
	}

	myopenlog(NAME, 0, LOG_LOCAL2);
	myloglevel(loglevel);

	/* block async signal delivery */
	sigset_t sigmask;
	sigfillset(&sigmask);
	if (sigprocmask(SIG_BLOCK, &sigmask, &orig_sigmask) < 0)
		mylog(LOG_ERR, "sigprocmask: %s", ESTR(errno));
	/* listen to queued signals */
	int sigfd = signalfd(-1, &sigmask, SFD_NONBLOCK | SFD_CLOEXEC);
	if (sigfd < 0)
		mylog(LOG_ERR, "signalfd failed: %s", ESTR(errno));
	libe_add_fd(sigfd, sigfd_handler, NULL);

	/* reconfigure */
	load_runtime_state(iface);
	newduid();

	sock = socket(PF_INET6, SOCK_DGRAM | SOCK_CLOEXEC, IPPROTO_UDP);
	if (sock < 0)
		mylog(LOG_ERR, "socket ipv6 udp failed: %s", ESTR(errno));
	if (setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, iface, strlen(iface)) < 0)
		mylog(LOG_ERR, "bindtodevice %s failed: %s", iface, ESTR(errno));
	opt = 1;
	if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0)
		mylog(LOG_ERR, "set-reuseaddr failed: %s", ESTR(errno));
	opt = 1;
	if (setsockopt(sock, SOL_IPV6, IPV6_V6ONLY, &opt, sizeof(opt)) < 0)
		mylog(LOG_ERR, "set-ipv6-only failed: %s", ESTR(errno));

	if (bind(sock, (void *)&local, sizeof(local)) < 0)
		mylog(LOG_ERR, "bind :: failed: %s", ESTR(errno));
	mylog(LOG_NOTICE, "bound to [%s]:%u", in6tostr(&local.sin6_addr), be16toh(local.sin6_port));
	libe_add_fd(sock, dhcpv6_handler, NULL);

	int rtnlsock = rtnl_listen_sock();
	if (rtnlsock < 0)
		mylog(LOG_ERR, "open netlink socket");
	libe_add_fd(rtnlsock, rtnl_link_change_handler, NULL);

	initiate_solicit();

	for (; !termreq;) {
		ret = libe_wait(libt_get_waittime());
		if (ret < 0 && errno == EINTR)
			continue;
		if (ret < 0)
			mylog(LOG_ERR, "libe_wait(): %s", ESTR(errno));
		libe_flush();
		libt_flush();
	}

	return 0;
}
