#include <errno.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <endian.h>
#include <fcntl.h>
#include <getopt.h>
#include <syslog.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "locallib.h"

#define NAME "modradvdconf"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

/* program options */
static const char help_msg[] =
	NAME ": modify radvd.conf for delegated prefixes\n"
	"usage:	" NAME " [OPTIONS ...] IFACE ...\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	" -c, --config=FILE	File to modify (/etc/radvd.conf)\n"
	" -p, --prefix=NET/BITS	delegated prefix\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },
	{ "config", required_argument, NULL, 'c', },
	{ "prefix", required_argument, NULL, 'p', },
	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "Vv?c:p:";

static char **cfgs;
static int ncfgs, scfgs;

static char *prefix;
static int pwidth;

/* config modifier */
static char *targetfile = "/etc/radvd.conf";
static char *workfile;
static struct stat st;
static int infd, outfd;
static FILE *fin;
static FILE *fout;

static inline void xfree(void *dat)
{
	if (dat)
		free(dat);
}

/* a strtok() replacement that does not touch the input string
 * the result comes from a local allocated buffer
 */
static char *safe_strtok(char *str, const char *sep)
{
	static char *saved_str;
	static char *sresult;
	char *result = NULL, *end = NULL;

	if (!str)
		str = saved_str;
	if (!str || !*str)
		return NULL;

	for (; *str; ++str) {
		if (strchr(sep, *str)) {
			if (result && !end)
				end = str;
		} else if (!result)
			/* start token here */
			result = str;
		else if (end)
			/* quit on valid char if token has already completed */
			break;
	}
	if (!end)
		end = str;
	saved_str = str;
	if (!result)
		return NULL;
	if (sresult)
		free(sresult);
	sresult = strndup(result, end-result);
	return sresult;
}

int main(int argc, char *argv[])
{
	int opt, ret;
	int loglevel = LOG_WARNING;
	char *line = NULL;
	size_t linesize = 0;
	char *tok, *curr_iface = NULL;
	int strip, j, iface_done;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\nCompiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case 'v':
		++loglevel;
		break;

	default:
		fprintf(stderr, "unknown option '%c'\n", opt);
	case '?':
		fputs(help_msg, stderr);
		exit(1);
		break;

	case 'c':
		targetfile = optarg;
		break;
	case 'p':
		prefix = strtok(optarg, "/");
		pwidth = strtoul(strtok(NULL, "/") ?: "64", NULL, 10);
		break;
	}

	myopenlog(NAME, 0, LOG_LOCAL2);
	myloglevel(loglevel);

	/* parse config arguments */
	if (!prefix)
		mylog(LOG_ERR, "no prefix given");
	opt = argc - optind;
	if (opt <= 0)
		mylog(LOG_WARNING, "no arguments given, removing all delegated prefixes");
	scfgs = opt*2+1;
	cfgs = malloc(sizeof(*cfgs)*scfgs);
	if (!cfgs)
		mylog(LOG_ERR, "malloc failed: %s", ESTR(errno));

	/* determine number of nets to do */
	int nnets = 1 << (64 - pwidth);
	if (nnets + optind > argc)
		nnets = argc - optind;
	else {
		mylog(LOG_WARNING, "%u nets provided, delegated prefix supports only %u",
				argc-optind, nnets);
		/* limit argc */
		argc = optind+nnets;
	}

	/* assign nets */
	struct in6_addr addr;
	if (!inet_pton(AF_INET6, prefix, &addr))
		mylog(LOG_ERR, "bad ipv6 prefix '%s'", prefix);

	uint64_t net = be64toh(*(uint64_t *)&addr);
	char buffer[128];

	for (; optind < argc; ++optind, ++net) {
		cfgs[ncfgs++] = argv[optind];

		*(uint64_t *)&addr = htobe64(net);
		inet_ntop(AF_INET6, &addr, buffer, sizeof(buffer));
		asprintf(&cfgs[ncfgs++], "%s/64", buffer);
	}
	cfgs[ncfgs] = NULL;

	/* prepare input file */
	infd = open(targetfile, O_RDONLY);
	if (infd < 0)
		mylog(LOG_ERR, "open %s: %s", targetfile, ESTR(errno));
	if (fstat(infd, &st) < 0)
		mylog(LOG_ERR, "fstat %s: %s", targetfile, ESTR(errno));
	fin = fdopen(infd, "r");
	if (!fin)
		mylog(LOG_ERR, "fstat %s: %s", targetfile, ESTR(errno));
	/* output file */
	asprintf(&workfile, "%s-XXXXXX", targetfile);
	outfd = mkstemp(workfile);
	if (outfd < 0)
		mylog(LOG_ERR, "mkstemp %s: %s", workfile, ESTR(errno));
	if (fchmod(outfd, st.st_mode) < 0) {
		mylog(LOG_WARNING, "fchmod %s 0%03o: %s", workfile, st.st_mode, ESTR(errno));
		goto failed;
	}
	fout = fdopen(outfd, "w");

	/* process files */
	strip = 0;
	for (;;) {
		ret = getline(&line, &linesize, fin);
		if (ret < 0 && feof(fin))
			break;
		if (ret < 0)
			mylog(LOG_ERR, "getline %s: %s", targetfile, ESTR(errno));
		tok = safe_strtok(line, " \t\r\n;");
		mylog(LOG_INFO, "token '%s'", tok);
		if (!tok)
			;
		else if (!strcmp(tok, "interface")) {
			xfree(curr_iface);
			curr_iface = strdup(safe_strtok(NULL, " \t\r\n;{"));
			iface_done = 0;
			mylog(LOG_INFO, "processing interface '%s'", curr_iface);
		} else if (!strcmp(tok, "#DELEGATED")) {
			strip = 1;
			continue;
		} else if (!strcmp(tok, "#DELEGATEDEND")) {
			if (iface_done)
				continue;
			/* flush delegated prefixes */
insert:
			fprintf(fout, "#DELEGATED\n");
			for (j = 0; j < ncfgs; j += 2) {
				if (!strcmp(cfgs[j], curr_iface ?: "")) {
					fprintf(fout, "\tprefix %s {};\n", cfgs[j+1]);
					/* mark as done */
					cfgs[j][0] = 0;
				}
			}
			fprintf(fout, "#DELEGATEDEND\n");
			iface_done = 1;
			if (strip) {
			mylog(LOG_INFO, "Delegated block inserted");
				/* stop stipping, but still strip this line */
				strip = 0;
				continue;
			}
		} else if (!strcmp(line, "};\n")) {
			if (!iface_done) {
				mylog(LOG_INFO, "no #DELEGATED block found, inserting at end of interface block for %s", curr_iface);
				/* do the same thing */
				goto insert;
			}
		} else if (strip)
			/* dont puts ... */
			continue;
		fputs(line, fout);
	}

	int nifaces = 0;
	for (j = 0; j < ncfgs; j += 2) {
		if (cfgs[j][0]) {
			mylog(LOG_WARNING, "prefix %s for iface '%s' not written", cfgs[j+1], cfgs[j]);
			++nifaces;
		}
	}
	if (nifaces)
		mylog(LOG_WARNING, "maybe verify %s", targetfile);

	/* replace target file */
	close(infd);
	if (fclose(fout) < 0) {
		mylog(LOG_WARNING, "fclose %s: %s", workfile, ESTR(errno));
		goto failed;
	}

	/* finally, replace file */
	if (rename(workfile, targetfile) < 0) {
		mylog(LOG_WARNING, "rename %s -> %s: %s", targetfile, workfile, ESTR(errno));
		goto failed;
	}
	return 0;
failed:
	if (unlink(workfile) < 0)
		mylog(LOG_WARNING, "unlink %s: %s", workfile, ESTR(errno));
	return 1;
}
