#include <errno.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <endian.h>
#include <fcntl.h>
#include <getopt.h>
#include <syslog.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "locallib.h"

#define NAME "ipv6netidx"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

/* program options */
static const char help_msg[] =
	NAME ": generate ipv6 network addres\n"
	"usage:	" NAME " [OPTIONS ...] PREFIX IDX\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },
	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "Vv?";

int main(int argc, char *argv[])
{
	int opt;
	int loglevel = LOG_WARNING;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\nCompiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case 'v':
		++loglevel;
		break;

	default:
		fprintf(stderr, "unknown option '%c'\n", opt);
	case '?':
		fputs(help_msg, stderr);
		exit(1);
		break;
	}

	myopenlog(NAME, 0, LOG_LOCAL2);
	myloglevel(loglevel);

	/* parse config arguments */
	if ((optind + 2) > argc) {
		fputs(help_msg, stderr);
		exit(1);
	}
	char *prefix = strtok(argv[optind++], "/");
	__attribute__((unused))
	int pwidth = strtoul(strtok(NULL, "/") ?: "64", NULL, 10);
	int idx = strtoul(argv[optind++], NULL, 0);

	/* assign nets */
	struct in6_addr addr;
	if (!inet_pton(AF_INET6, prefix, &addr))
		mylog(LOG_ERR, "bad ipv6 prefix '%s'", prefix);

	uint64_t net = be64toh(*(uint64_t *)&addr);
	net += idx;

	char buffer[128];
	*(uint64_t *)&addr = htobe64(net);
	inet_ntop(AF_INET6, &addr, buffer, sizeof(buffer));
	printf("%s\n", buffer);
	return 0;
}
